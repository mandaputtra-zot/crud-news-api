import 'regenerator-runtime/runtime'
import request from 'supertest'
import app from '../server'

describe('check route exist', () => {
  test('can create news', async () => {
    const res = await request(app)
    .post('/api/news').send({ title: 'Jakarta Banjir', content: 'Monas masih terendam' })
    expect(res.statusCode).toBe(200)
    expect(res.body).toBe({ title: 'Jakarta Banjir', content: 'Monas masih terendam' })
  })
})
