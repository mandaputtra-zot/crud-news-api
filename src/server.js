import 'regenerator-runtime/runtime'
import express from 'express'
import mongoose from 'mongoose'
import morgan from 'morgan'
import { urlencoded, json } from 'body-parser'

import { model } from './server.model'
import { getAll, createOne, removeOne, updateOne } from './server.controller'

const app = express()
const port = 8080

app.use(json())
app.use(urlencoded({ extended: true }))
app.use(morgan('dev'))

app.get('/api/news', getAll(model))
app.post('/api/news', createOne(model))
app.delete('/api/news', removeOne(model))
app.put('/api/news', updateOne(model))

export default app

app.listen(port, async () => {
  await mongoose.connect('mongodb://localhost:27017/news')
  console.log(`REST API on http://localhost:${port}/api/news`)
})
