import mongoose from 'mongoose'

const newsSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  content: String
})

export const model = mongoose.model('news', newsSchema)
