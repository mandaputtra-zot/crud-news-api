export const getAll = model => async (req, res) => {
  try {
    const docs = await model.find()

    if (!docs) {
      return res.status(400).json({ message: 'nothing in the database' })
    }

    return res.status(201).json({ data: docs })

  } catch (e) {
    console.error(e)
    return res.status(400).json({ message: "there's an error, abort mission" })
  }
}

export const createOne = model => async (req, res) => {
  try {
    const doc = await model.create(req.body)

    if (!doc) {
      return res.status(400).json({ message: 'nothing in the body, abort' })
    }

    return res.status(201).json({ data: doc })

  } catch (e) {
    console.error(e)
    return res.status(400).json({ message: 'failed' })
  }
}

// remove based on title
export const removeOne = model => async (req, res) => {
  try {
    const removed = await model.findOneAndRemove({
      title: req.body.title
    })
    return res.status(201).json({ data: removed })

  } catch (e) {
    console.error(e)
    return res.status(400).json({ message: 'failed' })
  }
}

// update the title
export const updateOne = model => async (req, res) => {
  try {
    const updated = await model.findOneAndUpdate(
      { title: req.body.title },
      { title: req.body.newTitle },
      { new: true })
    return res.status(200).json({ data: updated })

  } catch (e) {
    console.error(e)
    return res.status(400).json({ message: 'failed' })
  }
}
